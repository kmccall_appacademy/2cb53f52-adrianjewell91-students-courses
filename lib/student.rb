require 'byebug'

class Student

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  attr_accessor :first_name
  attr_accessor :last_name
  attr_accessor :courses

  def name
    @first_name + " " + @last_name
  end

  def enroll(course) #basically just modifies the course.
    #byebug
    raise "Error" if @courses.any?{|c| course.conflicts_with?(c)}

    unless @courses.include?(course)
      @courses << course
      course.students << self
    end
  end

  def course_load
    #byebug
    @load = Hash.new(0)

    @courses.each do |course|
      @load[course.department] += course.credits
    #print a hash equal to the number of credits in each dept.
    end
    @load
  end

end
